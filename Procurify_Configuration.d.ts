/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 */
import * as https from 'N/https';
export declare class Procurify_Configuration {
    static RECORDID: string;
    static NEWCONFIG: number;
    static NONONEWORLDSUBSIDIARYID: number;
    static FIELDID: {
        SUBSIDIARIES: string;
        PREFIX: string;
        DOMAIN: string;
        VERSION: string;
        USERNAME: string;
        PASSWORD: string;
        IMPORTFROM: string;
        RUNINSANDBOX: string;
        APILOGGING: string;
        DISCOUNT: {
            ITEM: string;
            DEPARTMENT: string;
            CLASS: string;
            LOCATION: string;
        };
        FREIGHT: {
            ITEM: string;
            DEPARTMENT: string;
            CLASS: string;
            LOCATION: string;
        };
        TAX: {
            ITEM: string;
            DEPARTMENT: string;
            CLASS: string;
            LOCATION: string;
        };
        OTHER: {
            ITEM: string;
            DEPARTMENT: string;
            CLASS: string;
            LOCATION: string;
        };
        ACCOUNT_ITEM: {
            TAX_SCHEDULE: string;
        };
    };
    private readonly recordId;
    private recordObj;
    private subsidiaries;
    private usedSubsidiaries;
    private readonly features;
    private readonly accountingPreferences;
    constructor(options: ConstructorOptions);
    get hasEvents(): boolean;
    get ProcurifyImportFrom(): string;
    get APILogging(): boolean;
    get ImportFrom(): string;
    set ImportFrom(importFrom: string);
    get RunInSandbox(): boolean;
    set RunInSandbox(runInSabdox: boolean);
    get Discount_Item(): number;
    set Discount_Item(itemId: number);
    get Discount_Department(): number;
    set Discount_Department(departmentId: number);
    get Discount_Class(): number;
    set Discount_Class(classId: number);
    get Discount_Location(): number;
    set Discount_Location(locationId: number);
    get Freight_Item(): number;
    set Freight_Item(itemId: number);
    get Freight_Department(): number;
    set Freight_Department(departmentId: number);
    get Freight_Class(): number;
    set Freight_Class(classId: number);
    get Freight_Location(): number;
    set Freight_Location(locationId: number);
    get Tax_Item(): number;
    set Tax_Item(itemId: number);
    get Tax_Department(): number;
    set Tax_Department(departmentId: number);
    get Tax_Class(): number;
    set Tax_Class(classId: number);
    get Tax_Location(): number;
    set Tax_Location(locationId: number);
    get Other_Item(): number;
    set Other_Item(itemId: number);
    get Other_Department(): number;
    set Other_Department(departmentId: number);
    get Other_Class(): number;
    set Other_Class(classId: number);
    get Other_Location(): number;
    set Other_Location(locationId: number);
    get AccountItem_TaxSchedule(): number;
    set AccountItem_TaxSchedule(taxScheduleId: number);
    get Id(): number;
    get Subsidiaries(): Subsidiary[];
    set Subsidiaries(subsidiaires: Subsidiary[]);
    get SubsidiaryIds(): number[];
    get SubsidiaryNames(): string[];
    get UsedSubsidiaries(): Subsidiary[];
    get UsedSubsidiariesIds(): number[];
    get AvailableSubsidiaries(): Subsidiary[];
    get UsableSubsidiaries(): Subsidiary[];
    get Prefix(): string;
    set Prefix(prefix: string);
    get Domain(): string;
    set Domain(domain: string);
    get Version(): string;
    set Version(version: string);
    get Username(): string;
    set Username(username: string);
    get Password(): string;
    set Password(password: string);
    get IsInActive(): boolean;
    set IsInActive(isInActive: boolean);
    get hasLocations(): boolean;
    get areLocationsMandatory(): boolean;
    get hasLocationsOnLines(): boolean;
    get hasDepartments(): boolean;
    get areDepartmentsMandatory(): boolean;
    get hasDepartmentsOnLines(): boolean;
    get hasClasses(): boolean;
    get areClassesMandatory(): boolean;
    get hasClassesOnLines(): boolean;
    get hasAdvancedTaxes(): boolean;
    get IsOneWorld(): boolean;
    get IsMultiCurrency(): boolean;
    get IsMultiCurrencyVendor(): boolean;
    static getConfigurationDetails(subsidiaryIds?: number[]): ConfigurationDetails[];
    static navigateToManager(response: https.ServerResponse): void;
    static navigateToSettings(response: https.ServerResponse, configId: number): void;
    static navigateToSubsidairyChooser(response: https.ServerResponse, configId: number): void;
    static delete(options: DeleteConfigurationOptions): void;
    static deleteDeployment(options: DeleteDeploymentOptions): void;
    static getTakenDomains(options: GetTakenValuesOptions): void;
    static getTakenPrefixes(options: GetTakenValuesOptions): void;
    createDeployments(configId: number): void;
    save(): number;
    private getUsedSubsidiaries;
    private loadRecord;
    private createDeployment;
    private findExistingDeployment;
}
export interface ConstructorOptions {
    configurationId?: number;
    subsidiaryIds?: number[];
}
export interface Subsidiary {
    id: number;
    name: string;
}
interface GetTakenValuesOptions {
    Found(values: string[]): void;
}
interface DeleteDeploymentOptions {
    exporter: boolean;
    objectTypeId: number;
    configId: number;
    Deleted(): void;
    Failed(): void;
}
interface DeleteConfigurationOptions {
    id: number;
    Deleted(): void;
    Failed(clientResponse: https.ClientResponse): void;
}
export interface ConfigurationDetails {
    id: number;
    domain: string;
}
export {};
