/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */
import { ProcurifySDK } from '../ProcurifySDK';
import { ItemReceiptLine } from '@extendapps/procurifyapi/Objects/ItemReceiptLine';
import { PurchaseOrder } from '@extendapps/procurifyapi/Objects/PurchaseOrder';
export declare namespace IImportHandler {
    namespace PurchaseOrderHandler {
        type create = (options: IPurchaseOrderCreateHandlerOptions) => void;
        type update = (options: IPurchaseOrderUpdateHandlerOptions) => void;
        type _delete = (options: IPurchaseOrderDeleteHandlerOptions) => void;
    }
    namespace ItemReceiptHandler {
        type create = (options: IItemReceiptCreateHandlerOptions) => void;
        type update = (options: IItemReceiptUpdateHandlerOptions) => void;
        type _delete = (options: IItemReceiptDeleteHandlerOptions) => void;
    }
}
export interface IImportHandler {
    create(options: IBaseCreateHandlerOptions): void;
    update(options: IBaseUpdateHandlerOptions): void;
    _delete(options: IBaseDeleteHandlerOptions): void;
}
export interface IBaseHandlerOptions {
    procurify: ProcurifySDK;
    eventId: number;
    object: any;
    Failed?(e: any, objectId: number): void;
}
export interface IBaseCreateHandlerOptions extends IBaseHandlerOptions {
    Created(recordId: number): void;
}
export interface IBaseUpdateHandlerOptions extends IBaseHandlerOptions {
    recordId: number;
    Updated(recordId: number): void;
}
export interface IBaseDeleteHandlerOptions extends IBaseHandlerOptions {
    recordId: number;
    Deleted(): void;
}
export interface IPurchaseOrderCreateHandlerOptions extends IBaseCreateHandlerOptions {
    object: PurchaseOrder;
}
export interface IPurchaseOrderUpdateHandlerOptions extends IBaseUpdateHandlerOptions {
    object: PurchaseOrder;
}
export interface IPurchaseOrderDeleteHandlerOptions extends IBaseDeleteHandlerOptions {
    object: PurchaseOrder;
}
export interface IItemReceiptCreateHandlerOptions extends IBaseCreateHandlerOptions {
    object: ItemReceiptLine[];
}
export interface IItemReceiptUpdateHandlerOptions extends IBaseUpdateHandlerOptions {
    object: ItemReceiptLine[];
}
export interface IItemReceiptDeleteHandlerOptions extends IBaseDeleteHandlerOptions {
    object: ItemReceiptLine[];
}
