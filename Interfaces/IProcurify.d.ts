/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */
import * as search from 'N/search';
export interface lookingForValueOptions {
    netsuiteName: string;
    procurifyName: string;
    vendorName: string;
}
export interface RecordLookupOptions {
    searchOptions?: search.SearchCreateOptions;
    recordId?: number;
    objectId?: number;
    isMandatory: boolean;
    lookingForValue: lookingForValueOptions;
    Found(result: search.Result): void;
}
export interface GetCountryCodeOptions {
    country: string;
    Found(countryCode: string): void;
    NotFound(): void;
}
