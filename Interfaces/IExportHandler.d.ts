/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */
import * as record from 'N/record';
import { Vendor } from '@extendapps/procurifyapi/Objects/Vendor';
import { Account } from '@extendapps/procurifyapi/Objects/Account';
import { ProcurifySDK } from '../ProcurifySDK';
import * as https from 'N/https';
import { PaymentTerm } from '@extendapps/procurifyapi/Objects/PaymentTerm';
import { CatalogItem } from '@extendapps/procurifyapi/Objects/CatalogItem';
export declare namespace IExportHandler {
    namespace AccountHandler {
        type handler = (options: IBaseExportAccountHandlerOptions) => void;
    }
    namespace VendorHandler {
        type handler = (options: IBaseExportVendorHandlerOptions) => void;
    }
    namespace PaymentTermHandler {
        type handler = (options: IBaseExportPaymentTermHandlerOptions) => void;
    }
    namespace CatalogItemHandler {
        type handler = (options: IBaseExportCatalogItemHandlerOptions) => void;
    }
}
export interface IExportHandler {
    handler(options: IBaseExportHandlerOptions): void;
}
export interface IBaseExportHandlerOptions {
    procurify: ProcurifySDK;
    netSuiteRecord: record.Record;
    Send(object: any): void;
    Failed?(clientResponse: https.ClientResponse): void;
}
export interface IBaseExportAccountHandler extends IExportHandler {
    handler(options: IBaseExportAccountHandlerOptions): void;
}
export interface IBaseExportAccountHandlerOptions extends IBaseExportHandlerOptions {
    procurifyAccountId: number;
    Send(account: Account): void;
}
export interface IBaseExportVendorHandler extends IExportHandler {
    handler(options: IBaseExportVendorHandlerOptions): void;
}
export interface IBaseExportVendorHandlerOptions extends IBaseExportHandlerOptions {
    procurifyVendorId: number;
    Send(vendor: Vendor): void;
}
export interface IBaseExportPaymentTermHandler extends IExportHandler {
    handler(options: IBaseExportPaymentTermHandlerOptions): void;
}
export interface IBaseExportPaymentTermHandlerOptions extends IBaseExportHandlerOptions {
    procurifyPaymentTermId: number;
    Send(paymentTerm: PaymentTerm): void;
}
export interface IBaseExportCatalogItemHandler extends IExportHandler {
    handler(options: IBaseExportCatalogItemHandlerOptions): void;
}
export interface IBaseExportCatalogItemHandlerOptions extends IBaseExportHandlerOptions {
    procurifyCatalogItemId: number;
    Send(catalogItem: CatalogItem): void;
}
