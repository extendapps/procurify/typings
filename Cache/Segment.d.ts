/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import * as search from 'N/search';
import * as record from 'N/record';
export interface ManageSegmentOptions {
    name?: string;
    segmentId?: number;
    recordObj: record.Record;
    segment: search.Type;
    sublistId?: string;
}
