/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
export declare const getNetSuiteAccountDetail: (options: GetNetSuiteAccountDetail) => void;
export declare const cacheAccounts: (configurationId: number) => void;
export interface NetSuiteAccountDetail {
    procurifyId?: number;
    netSuiteId: number;
    number: string;
    custrecord_procurify_acct_item: number;
    isinactive: boolean;
    configurationId: number;
}
export interface GetNetSuiteAccountDetail {
    number?: string;
    procurifyId?: number;
    configurationId: number;
    Found(netSuiteAccountDetail: NetSuiteAccountDetail): void;
    NotFound(): void;
}
