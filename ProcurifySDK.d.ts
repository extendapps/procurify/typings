/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 */
import { Procurify_Configuration } from './Procurify_Configuration';
import { GetNetSuiteAccountDetail } from './Cache/Account';
import { GetCountryCodeOptions, RecordLookupOptions } from './Interfaces/IProcurify';
import { AccountType, ProcurifyAPI } from '@extendapps/procurifyapi/procurifyapi';
import * as record from "N/record";
import * as search from "N/search";

export interface ProcurifySDK {
    readonly Configuration: Procurify_Configuration;
    readonly API: ProcurifyAPI;
    readonly hasEvents: boolean;
    readonly isConnected: boolean;
    getCurrencyNetsuiteName(currencyCode: string): string;
    getNetSuiteAccountDetail(options: GetNetSuiteAccountDetail): void;
    getCurrencyId(currencyCode: string): number;
    getCountryCode(options: GetCountryCodeOptions): void;
    getAccountType(netSuiteAccountType: string): AccountType;
    manageSegment(options: ManageSegmentOptions): void;
    procurifyDatetoNSDate(procurifyDate: string): Date;
    recordLookup(options: RecordLookupOptions): void;
}

export interface ManageSegmentOptions {
    name?: string;
    segmentId?: number;
    recordObj: record.Record;
    segment: search.Type;
    sublistId?: string;
}
