/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */
import * as record from 'N/record';
import { IImportHandler } from '../../Interfaces/IImportHandler';
import { ProcurifySDK } from '../../ProcurifySDK';
import { Address } from '@extendapps/procurifyapi/Objects/Address';
export declare const update: IImportHandler.PurchaseOrderHandler.update;
export declare const _delete: IImportHandler.PurchaseOrderHandler._delete;
export declare const create: IImportHandler.PurchaseOrderHandler.create;
export declare const setAddress: (transaction: record.Record, address: Address, procurify: ProcurifySDK, isBilling?: boolean) => void;
