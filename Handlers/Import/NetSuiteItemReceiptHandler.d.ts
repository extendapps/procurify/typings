/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */
import { IImportHandler } from '../../Interfaces/IImportHandler';
export declare const update: IImportHandler.ItemReceiptHandler.update;
export declare const _delete: IImportHandler.ItemReceiptHandler._delete;
export declare const create: IImportHandler.ItemReceiptHandler.create;
