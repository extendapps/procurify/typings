/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */
import { IExportHandler } from '../../Interfaces/IExportHandler';
export declare const handler: IExportHandler.VendorHandler.handler;
